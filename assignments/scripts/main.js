console.log("I am in main.js");
// Const is keyword. When value is not changing, we have to use keyword const
/**
 * new operator will create new object of class { Date }
 */
const currentDate = new Date();

// 2 & 4 are tuesday and thursday
const webDevClasses = [2, 4];

// 1,3,5 - 
const testingClasses = [1, 3, 5];

// This is function call or execution
printMessage("current Date is : " + currentDate);
printMessage("current Date is : " + currentDate, 'info');
printMessage("current Date is : " + currentDate, 'error');

const halloweenWeekend = new Date(2019, 10, 2, 0, 0, 0, 0);
const isHoliday = isWeekend(halloweenWeekend);
if (isHoliday === true) {
    printMessage("Store is closed", "error");
} else {
    printMessage("Store is Open");
}

/**
 * print error log message when type is error
 * print info log message when type is info
 * @param {string} message 
 */
// This is function defination
function printMessage(message, type) {
    if (type === 'info') {
        console.info(message);
    } else if (type === 'error') {
        console.error(message);
    } else {
        console.log(message);
    }
}

// Find weekend based on the date
/**
 * when day is sunday or saturday, its weekend
 */

/**
 * 
 * @param {Date} date 
 * @returns boolean
 */
function isWeekend(date) {
    const dayNumber = date.getDay();
    if (dayNumber === 0 || dayNumber === 6) {
        return true;
    } else {
        return false;
    }
}

function isOpenHours(date) {
    const hours = date.getHours();
    const minutes = date.getMinutes();
    // When time is 6:30pm to 8:30pm, return true
    if ((hours === 18 && minutes >= 30) || (hours === 20 && minutes <= 30)) {
        return true;
    }
    return false;
}

function isOpenDays(date, openDays) {
    const dayNumber = date.getDay();
    // Return true when day is Tuesday or Thursday

    //if (dayNumber === day1 || dayNumber === day2) {
    // [2,4] when dayNumber 
    if (openDays.indexOf(dayNumber)) {
        return true;
    } else {
        return false;
    }
}

function isWebDevClassOpen(date) {
    const isWorkingDay = isOpenDays(date, webDevClasses);
    const isWorkingHours = isOpenHours(date);
    if (isWorkingDay && isWorkingHours) {
        return true;
    }
    return false;
}



const isWebDevSessionInProgress = isWebDevClassOpen(currentDate);
if (isWebDevSessionInProgress) {
    printMessage("Webdevelopment class is in progress");
}
print("Webdevelopment class is closed");

